#!/usr/bin/env bash

PUBLIC_URI=${PUBLIC_URI:-}
PROTOCOL=${PROTOCOL:-https}
DOMAIN=${DOMAIN:-rabrux.space}

rm -rf public
mkdir public
cp -r src/* public/

for file in $(find public/ -type f -name *.html)
do
  template=`cat "$file"`
  template=`echo "${template//<%PUBLIC_URI%>/${PUBLIC_URI}}"`

  for uri in $(echo -e "$template" | grep -Eo "<%BUILD_URI%>[^\"]*" | uniq)
  do
    [[ "$uri" =~ \<%BUILD_URI%\>(.*) ]] || continue
    [[ "${BASH_REMATCH[1]}" = "" ]] && compiled_uri=`echo "$PROTOCOL://${DOMAIN}"` || compiled_uri=`echo "$PROTOCOL://${BASH_REMATCH[1]}.${DOMAIN}"`
    template=`echo "${template//\"${BASH_REMATCH[0]}\"/\"${compiled_uri}\"}"`
  done

  echo -e "$template" > "$file"
done
